# cpp-wasm-talk

Slides and code for my [cpp user group talk on c++ webassembly](https://git.gsi.de/SDE/cxx-user-group/-/issues/38).

# Sample code
to run the samples see the instructions in the respective subdirectories:
- 01_first_steps

## Slides
compile the slides with webassembly:
```
emcmake cmake -B build-wasm -S slides-wasm && cmake --build build-wasm -j
python3 -m http.server 8080 &
xdg-open localhost:8080
```
Compiled version at [http://web-docs.gsi.de/~akrimm/wasm-cpp/](http://web-docs.gsi.de/~akrimm/wasm-cpp/).

compile the slides to native:
```
cmake -B build-native -S slides-wasm && cmake --build build-native -j
build-native/wasm-cpp-talk
```
