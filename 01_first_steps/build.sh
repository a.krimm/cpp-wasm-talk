emcc wasm01.cpp -o index.html -s EXPORTED_RUNTIME_METHODS=['UTF8ToString']

# use vanilla clang -> no emscripten.h, no stdlib (because stdlib needs posix)
# clang++ wasm01.cpp --target=wasm32 -o clang.wasm
