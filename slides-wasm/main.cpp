#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>
#include <implot.h>
#include <SDL.h>
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <SDL_opengles2.h>
#else
#include <SDL_opengl.h>
#endif
#include <TextEditor.h>
#include <cstdio>
#include <fmt/core.h>

#ifdef __EMSCRIPTEN__
EM_JS(void, helloAlert, (const char* str), {
    alert(Module.UTF8ToString(str));
});
#endif

// Emscripten requires to have full control over the main loop. We're going to
// store our SDL bookkeeping variables globally. Having a single function that
// acts as a loop prevents us to store state in the stack of said function. So
// we need some location for this.
SDL_Window   *g_Window    = NULL;
SDL_GLContext g_GLContext = NULL;
bool          running     = true;
const int     nslides     = 12;
int           slide       = 0;
int           anim        = 0;
const int     nAnim[12]   = {1,1,4,1,5,5,1,4,1,1,1,1};
bool          refresh     = true;

// fonts
ImFont *fontDefault;
ImFont *fontTitle;
ImFont *fontBig;
ImFont *fontFooter;
ImFont *fontMono;

// text editor is stateful, max 3 editors per slide
TextEditor editor;

// images
int img_emscripten_w = 0;
int img_emscripten_h = 0;
GLuint img_emscripten_tex=0;
int img_wa_w = 0;
int img_wa_h = 0;
GLuint img_wa_tex=0;
int img_qt_w = 0;
int img_qt_h = 0;
GLuint img_qt_tex=0;
//int img__w = 0;
//int img__h = 0;
//GLuint img__tex=0;

bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height);
void TextCentered(const std::string& text);
static void   main_loop(void *);
static void RenderCode(const char *code, TextEditor::LanguageDefinition definition);
static void ClickableUrl(const char *fmt, const char *url);
void Demo_RealtimePlots();

static void handleSdlEvents();

int           main(int, char **) {
    // Setup SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }

    // For the browser using Emscripten, we are going to use WebGL1 with GL ES2.
    // It is very likely the generated file won't work in many browsers.
    // Firefox is the only sure bet, but I have successfully run this code on
    // Chrome for Android for example.
    const char *glsl_version = "#version 100";
    // const char* glsl_version = "#version 300 es";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_DisplayMode current;
    SDL_GetCurrentDisplayMode(0, &current);
    SDL_WindowFlags window_flags = (SDL_WindowFlags) (SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    g_Window                     = SDL_CreateWindow("GSI C++ Usergroup Talk - WebAssembly in C++", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    g_GLContext                  = SDL_GL_CreateContext(g_Window);
    if (!g_GLContext) {
        fprintf(stderr, "Failed to initialize WebGL context!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();
    ImGuiIO &io = ImGui::GetIO();

    // For an Emscripten build we are disabling file-system access, so let's not
    // attempt to do a fopen() of the imgui.ini file. You may manually call
    // LoadIniSettingsFromMemory() to load settings from your own storage.
    io.IniFilename = NULL;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsClassic();
    // ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplSDL2_InitForOpenGL(g_Window, g_GLContext);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    //io.Fonts->AddFontDefault();
#ifndef IMGUI_DISABLE_FILE_FUNCTIONS
    ImVector<ImWchar> symbols;
    ImFontGlyphRangesBuilder builder;
    builder.AddText("⋅∘");
    builder.BuildRanges(&symbols);
    ImFontConfig cfg_merge;
    cfg_merge.MergeMode = true;

    fontTitle = io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 104.0f);
    io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 104.0f, &cfg_merge, symbols.Data);
    fontBig = io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 64.0f);
    io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 64.0f, &cfg_merge, symbols.Data);
    fontFooter = io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 32.0f);
    io.Fonts->AddFontFromFileTTF("asset_dir/Vollkorn-Regular.ttf", 32.0f, &cfg_merge, symbols.Data);
    fontMono = io.Fonts->AddFontFromFileTTF("asset_dir/B612Mono-Regular.ttf", 32.0f);
    fontDefault = io.Fonts->AddFontDefault();
    io.Fonts->Build();
    // IM_ASSERT(font != NULL);
#endif

    // load images
    [[maybe_unused]] bool ret = LoadTextureFromFile("asset_dir/Emscripten_logo_full.png", &img_emscripten_tex, &img_emscripten_w, &img_emscripten_h);
    IM_ASSERT(ret);
    [[maybe_unused]] bool ret2 = LoadTextureFromFile("asset_dir/WebAssembly_Logo.png", &img_wa_tex, &img_wa_w, &img_wa_h);
    IM_ASSERT(ret2);
    [[maybe_unused]] bool ret3 = LoadTextureFromFile("asset_dir/QtDemo.png", &img_qt_tex, &img_qt_w, &img_qt_h);
    IM_ASSERT(ret3);

    // setup code editor
    editor.SetReadOnly(true);

    // This function call won't return, and will engage in an infinite loop, processing events from the browser, and dispatching them.
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop_arg(main_loop, NULL, 0, true);
    SDL_GL_SetSwapInterval(1); // Enable vsync
#else
    SDL_GL_SetSwapInterval(1); // Enable vsync
    while (running) {
        main_loop(NULL);
    }
    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(g_GLContext);
    SDL_DestroyWindow(g_Window);
    SDL_Quit();
#endif
    // emscripten_set_main_loop_timing(EM_TIMING_SETIMMEDIATE, 10);
}

static void main_loop(void *arg) {
    ImGuiIO &io = ImGui::GetIO();
    IM_UNUSED(arg); // We can pass this argument as the second parameter of emscripten_set_main_loop_arg(), but we don't use that.

    // Our state (make them static = more or less global) as a convenience to keep the example terse.
    static ImVec4 clear_color      = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // Poll and handle events (inputs, window resize, etc.)
    handleSdlEvents();

    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    // fullscreen window
    {
        static bool use_work_area = true;
        static ImGuiWindowFlags flags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings;
        // We demonstrate using the full viewport area or the work area (without menu-bars, task-bars etc.)
        // Based on your use case you may want one of the other.
        const ImGuiViewport *viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(use_work_area ? viewport->WorkPos : viewport->Pos);
        ImGui::SetNextWindowSize(use_work_area ? viewport->WorkSize : viewport->Size);
        bool p_open = true;
        if (ImGui::Begin("Example: Fullscreen window", &p_open, flags)) {
            switch (slide) {
                case 0: // title slide
                    ImGui::SetCursorPosY(static_cast<float>(viewport->Size.y * 0.2));
                    ImGui::PushFont(fontTitle);
                    TextCentered("WebAssembly in C++");
                    TextCentered("Compiling C++ for the web and more");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    TextCentered("Alexander Krimm - GSI C++ User Group Meeting 2022-48");
                    ImGui::PopFont();
                    break;
                case 1: // first slide
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("Outline");
                    ImGui::PopFont();
                    ImGui::Indent(32);
                    ImGui::PushFont(fontBig);
                    TextCentered("history and overview");
                    TextCentered("minimal example");
                    TextCentered("differences to native/POSIX");
                    TextCentered("available APIs");
                    TextCentered("UI");
                    ImGui::PushFont(fontFooter);
                    TextCentered("ImGui");
                    TextCentered("Qt");
                    ImGui::PopFont();
                    TextCentered("Webassembly outside of the Browser");
                    TextCentered("Time for some demos");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 2: // Overview
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("What is WebAssembly, History and Overview");
                    ImGui::PopFont();
                    ImGui::Indent(32);
                    ImGui::PushFont(fontBig);
                    if (anim >=1) {
                        ImGui::Text("⋅ loads of JavaScript -> jit-compilation -> distributing precompiled js");
                    }
                    if (anim >=2) {
                        ImGui::Text("⋅ binary/textual format for compiled code");
                        ImGui::Text("⋅ VM to execute such code");
                        ImGui::Image((void *) (intptr_t) img_wa_tex, ImVec2(img_wa_w, img_wa_h));
                    }
                    if (anim >=3) {
                        ImGui::Text("⋅ greater ecosystem: emscripten toolchain = clang/llvm \n      + Binaryen + posix/sdl/openGL compatibility layer");
                        ImGui::Image((void*)(intptr_t)img_emscripten_tex, ImVec2(img_emscripten_w, img_emscripten_h));
                    }
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 3:
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("... and more");
                    ImGui::PopFont();
                    ImGui::PushFont(fontDefault);
                    Demo_RealtimePlots();
                    ImGui::PopFont();
                    break;
                case 4: // minimal example
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("Minimal Example");
                    ImGui::PopFont();
                    if (anim == 1){
                        RenderCode(R"VOGON(#include <emscripten.h>
#include <iostream>

EM_JS(void, helloAlert, (const char* str), {
    alert(Module.UTF8ToString(str));
});

int main() {
    helloAlert("Finally we can `alert()` from C++");
    std::cout << "Hello World\n";
    return 42;
}
)VOGON", TextEditor::LanguageDefinition::CPlusPlus());
                    }
                    if (anim == 2) {
                        RenderCode(R"VOGON(<html>
  <head> <title>wasm shell</title> </head>
  <body>
    <textarea id="out"></textarea>

    <script type='text/javascript'>
      var elem = document.getElementById('out');
      var Module = {
        print: (function(txt) {
          console.log(txt);
          elem.value += txt + "\n";
        }),
      };
    </script>
    <script async type="text/javascript" src="index.js"></script>
  </body>
</html>
)VOGON", TextEditor::LanguageDefinition());
                    }
                    if (anim >= 3) {
                        RenderCode(R"VOGON(emcc wasm01.cpp -o index.html -s -s EXPORTED_RUNTIME_METHODS=['UTF8ToString']
python3 -m http.server 8080
)VOGON", TextEditor::LanguageDefinition());
                    }
#ifdef __EMSCRIPTEN__
                    if (anim == 4 && refresh) helloAlert("Finally we can `alert()` from C++");
#endif
                    break;
                case 5: // differences native/posix vs emscripten
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("WebAssembly vs native/POSIX");
                    ImGui::PopFont();
                    ImGui::Indent(32);
                    ImGui::PushFont(fontBig);
                    if (anim >=1) {
                        ImGui::Text("⋅ main function");
                        ImGui::Indent(32);
                        ImGui::Text("⋅ browsers use cooperative multitasking -> main has to return");
                        ImGui::Text("⋅ use set_emscripten_main_loop");
                        ImGui::Text("⋅ Asyncify: special emscripten_sleep() call returns control to the browser");
                        ImGui::Unindent(32);
                    }
                    if (anim == 1) {
                        RenderCode(R"VOGON(void main() {
    initUI();
    while (running) {
        handle(events);
        renderUI();
        waitForNextFrame(60.0);
    }
}
)VOGON", TextEditor::LanguageDefinition::CPlusPlus());
                    }
                    if (anim == 2) {
                        RenderCode(R"VOGON(void main_loop() {
    handle(events);
    renderUI();
}

void main() {
    initUI();
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(main_loop, 60.0, false);
#else
    while (running) {
        main_loop();
        waitForNextFrame(60.0);
        // emscripten_sleep(20) // alternative Asyncify approach
    }
#endif
}
)VOGON", TextEditor::LanguageDefinition::CPlusPlus());
                    }
                    if (anim >=3) {
                        ImGui::Text("⋅filesystem");
                        ImGui::Indent(32);
                        ImGui::Text("⋅ posix (file) io is mostly synchronous -> would block browser thread");
                        ImGui::Text("⋅ emscripten provides virtual filesystem, populated asynchronously at launch");
                        ImGui::PushFont(fontMono);
                        ImGui::Text("  emcc file.cpp -o file.html --preload-file asset_dir");
                        ImGui::PopFont();
                        ImGui::Text("⋅or directly use asynchronous file operations");
                        ImGui::Unindent(32);
                    }
                    if (anim >=4) {
                        ImGui::Text("⋅ browser limitations");
                        ImGui::Indent(32);
                        ImGui::Text("⋅ different memory management, controlled by browser");
                        ImGui::Text("⋅ some APIs are subject to e.g. same-origin policies -> needs e.g. HTTP-Headers");
                        ImGui::Unindent(32);
                    }
                    //if (anim == 3) {
                    //    RenderCode(R"VOGON(
//)VOGON", TextEditor::LanguageDefinition::CPlusPlus());
                    //}
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 6: // APIs
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("Available emscripten APIs");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Indent(32);
                    ImGui::Text("⋅ C++ Standard Library");
                    ImGui::Text("⋅ SDL -> HTML5 canvas");
                    ImGui::Text("⋅ html5.h -> low-level canvas");
                    ImGui::Text("⋅ EGL/OpenGL -> WebGL");
                    ImGui::Text("⋅ OpenAL -> WebAudio");
                    ImGui::Text("⋅ Pthreads");
                    ImGui::Text("⋅ WebSockets & WebRTC");
                    ImGui::Text("⋅ SIMD");
                    ImGui::Dummy({0.0f, 16.0f});
                    ImGui::Text("⋅ see API documentation for required emcc flags");
                    ImGui::Text("⋅ ... and lots of compatible 3rd party frameworks/libraries");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 7: // imgui
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("UI - ImGui");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Indent(32);
                    ImGui::Text("⋅Immediate-mode GUI toolkit with a big ecosystem of extension libraries (plotting, node editors, etc)");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    RenderCode(R"VOGON(// Create a window called "My First Tool", with a menu bar.
ImGui::Begin("My First Tool", &my_tool_active, ImGuiWindowFlags_MenuBar);
if (ImGui::BeginMenuBar()) {
    if (ImGui::BeginMenu("File")) {
        if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* Do stuff */ }
        if (ImGui::MenuItem("Close", "Ctrl+W"))  { my_tool_active = false; }
        ImGui::EndMenu();
    }
    ImGui::EndMenuBar();
}
ImGui::ColorEdit4("Color", my_color); // Edit a color stored as 4 floats

// Generate samples and plot them
float samples[100];
for (int n = 0; n < 100; n++)
    samples[n] = sinf(n * 0.2f + ImGui::GetTime() * 1.5f);
ImGui::PlotLines("Samples", samples, 100);

// Display contents in a scrolling region
ImGui::TextColored(ImVec4(1,1,0,1), "Important Stuff");
ImGui::BeginChild("Scrolling");
for (int n = 0; n < 50; n++)
    ImGui::Text("%04d: Some text", n);
ImGui::EndChild();
ImGui::End();
)VOGON", TextEditor::LanguageDefinition::CPlusPlus());
                    if (anim >= 1) {
                        ImGui::PushFont(fontDefault);
                        static float my_color[4];
                        static bool my_tool_active = true;
                        // Create a window called "My First Tool", with a menu bar.
                        ImGui::SetNextWindowSize({300,400});
                        ImGui::Begin("My First Tool", &my_tool_active, ImGuiWindowFlags_MenuBar);
                        if (ImGui::BeginMenuBar())
                        {
                            if (ImGui::BeginMenu("File"))
                            {
                                if (ImGui::MenuItem("Open..", "Ctrl+O")) { /* Do stuff */ }
                                if (ImGui::MenuItem("Save", "Ctrl+S"))   { /* Do stuff */ }
                                if (ImGui::MenuItem("Close", "Ctrl+W"))  { my_tool_active = false; }
                                ImGui::EndMenu();
                            }
                            ImGui::EndMenuBar();
                        }

// Edit a color stored as 4 floats
                        ImGui::ColorEdit4("Color", my_color);

// Generate samples and plot them
                        float samples[100];
                        for (int n = 0; n < 100; n++)
                            samples[n] = sinf(n * 0.2f + ImGui::GetTime() * 1.5f);
                        ImGui::PlotLines("Samples", samples, 100);

// Display contents in a scrolling region
                        ImGui::TextColored(ImVec4(1,1,0,1), "Important Stuff");
                        ImGui::BeginChild("Scrolling");
                        for (int n = 0; n < 50; n++)
                            ImGui::Text("%04d: Some text", n);
                        ImGui::EndChild();
                        ImGui::End();
                        ImGui::PopFont();
                    }
                    if (anim >= 2) {
                        ImGui::PushFont(fontDefault);
                        ImGui::SetNextWindowFocus(); // ensure that demo window has focus
                        ImGui::ShowDemoWindow(&p_open);
                        ImGui::PopFont();
                    }
                    if (anim >= 3) {
                        ImGui::PushFont(fontDefault);
                        ImGui::SetNextWindowFocus(); // ensure that demo window has focus
                        ImPlot::ShowDemoWindow(&p_open);
                        ImGui::PopFont();
                    }
                    break;
                case 8: // qt
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("UI - Qt");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Indent(32);
                    ImGui::Text("⋅ most Qt6 modules are emscripten compatible");
                    ImGui::Text("⋅ Qt's abstraction + buildsystem leads to even less code changes");
                    ImGui::Text("⋅ downside: size > 10MiB even for small examples");
                    ImGui::Image((void*)(intptr_t)img_qt_tex, ImVec2(img_qt_w, img_qt_h));
                    if (ImGui::IsItemClicked(ImGuiMouseButton_Left)) SDL_OpenURL("https://www.qt.io/web-assembly-example-slate");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 9: // non-web
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("Webassembly outside of the browser");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Indent(32);
                    ImGui::Text("⋅ WebAssembly is not limited to the browser");
                    ImGui::Text("⋅ wasm runtimes: nodejs, wasmtime, wasmer");
                    ImGui::Text("⋅ native API: WASI");
                    ImGui::Text("⋅ WAPM.io: package manager/repository for wasm apps and libraries");
                    ImGui::Text("⋅ embedded runtimes for e.g. plug-ins");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 10: // demo-time
                    ImGui::PushFont(fontTitle);
                    ImGui::Text("Time for some more demos!");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Indent(32);
                    ClickableUrl("∘ implot webAssembly demo: %s", "https://traineq.org/implot_demo/src/implot_demo.html");
                    ClickableUrl("∘ Qt: %s", "https://www.qt.io/web-assembly-example-slate");
                    ClickableUrl("∘ chart-qt: %s", "https://web-docs.gsi.de/~akrimm/chart-qt/chart-qt.html");
                    ClickableUrl("∘ magnum showcase: %s", "https://magnum.graphics/showcase/");
                    ClickableUrl("∘ WAPM.io package repository: %s", "https://wapm.io/explore");
                    ImGui::Unindent(32);
                    ImGui::PopFont();
                    break;
                case 11: // End Slide
                default:
                    ImGui::SetCursorPosY(static_cast<float>(viewport->Size.y * 0.15));
                    ImGui::PushFont(fontTitle);
                    TextCentered("Questions?");
                    ImGui::PopFont();
                    ImGui::PushFont(fontBig);
                    ImGui::Text("References");
                    ImGui::Indent(32);
                    ClickableUrl("∘ This presentation: %s", "https://web-docs.gsi.de/~akrimm/wasm-cpp/");
                    ClickableUrl("   ∘ code: %s", "https://git.gsi.de/a.krimm/cpp-wasm-talk");
                    ClickableUrl("∘ WebAssembly standard: %s", "https://webassembly.org/");
                    ClickableUrl("∘ emscripten documentation: %s", "https://emscripten.org/");
                    ClickableUrl("∘ wasm with vanilla clang: %s", "https://surma.dev/things/c-to-webassembly/");
                    ClickableUrl("∘ Qt-docs: %s", "https://doc.qt.io/qt6/wasm.html");
                    ImGui::Unindent(32);
                    ImGui::PopFont();

                    ImGui::SetCursorPos({16, viewport->Size.y - 72});
                    ImGui::PushFont(fontFooter);
                    ImGui::Text("Presentation built using emscripten, imgui, implot, ImGuiColorTextEdit and sbt. Fonts: Vollkorn & B612Mono");
                    ImGui::PopFont();
                    break;
            }
            // footer
            ImGui::SetCursorPos({16, viewport->Size.y - 32});
            ImGui::PushFont(fontFooter);
            ImGui::Text("GSI C++ User Group Meeting 2022-48  -  WebAssembly in C++");
            ImGui::SameLine(viewport->GetCenter().x - 64);
            ImGui::Text("Alexander Krimm");
            ImGui::SameLine(viewport->Size.x - 64);
            ImGui::Text("%02d/%02d", slide+1, nslides);
            ImGui::PopFont();
        }
        ImGui::End();
    }

    refresh = false;
    // Rendering
    ImGui::Render();
    SDL_GL_MakeCurrent(g_Window, g_GLContext);
    glViewport(0, 0, (int) io.DisplaySize.x, (int) io.DisplaySize.y);
    glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    SDL_GL_SwapWindow(g_Window);
}

static void handleSdlEvents() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        ImGui_ImplSDL2_ProcessEvent(&event);
        if (event.type == SDL_KEYDOWN || /*event.type == SDL_MOUSEBUTTONDOWN ||*/ event.type == SDL_MOUSEWHEEL) {
            if (event.type == SDL_KEYDOWN) {
                switch (event.key.keysym.sym) {
                    case SDLK_RIGHT:
                    case SDLK_SPACE:
                        anim++;
                        refresh = true;
                        break;
                    case SDLK_UP:
                        slide++;
                        refresh = true;
                        break;
                    case SDLK_LEFT:
                        anim--;
                        refresh = true;
                        break;
                    case SDLK_DOWN:
                        slide--;
                        refresh = true;
                        break;
                }
            }
            //if (event.type == SDL_MOUSEBUTTONDOWN) {
            //    if (event.button.button == SDL_BUTTON_LEFT) {
            //        anim++;
            //        refresh = true;
            //    } else if (event.button.button == SDL_BUTTON_RIGHT) {
            //        anim--;
            //        refresh = true;
            //    }
            //}
            if (event.type == SDL_MOUSEWHEEL) {
                if (event.wheel.y < 0) {
                    anim++;
                    refresh = true;
                } else {
                    anim--;
                    refresh = true;
                }
            }
            if (anim >= nAnim[slide]) {
                anim = 0;
                slide++;
            }
            if (anim < 0) {
                slide--;
                if (slide < 0) {
                    slide = 0;
                    anim = 0;
                } else {
                    anim = nAnim[slide] - 1;
                }
            }
            if (slide < 0) slide = 0;
            if (slide >= nslides) {
                slide = nslides -1;
                anim = nAnim[slide] - 1;
            }
            continue;
        }
        if (event.type == SDL_QUIT)
            running = false;
        if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(g_Window))
            running = false;
    }
}

static void ClickableUrl(const char *fmt, const char *url) {
    ImGui::Text(fmt, url);
    if (ImGui::IsItemClicked(ImGuiMouseButton_Left)) SDL_OpenURL(url);
}

static void RenderCode(const char *code, const TextEditor::LanguageDefinition definition) {
    ImGui::PushFont(fontMono);
    auto size = ImGui::CalcTextSize(code);
    ImGui::BeginChild("Code", {size.x+96, size.y+48}, false, ImGuiWindowFlags_NoDecoration);
    if (refresh) {
        editor.SetLanguageDefinition(definition);
        editor.SetText(code);
    }
    editor.Render("code");
    ImGui::EndChild();
    ImGui::PopFont();
}

void TextCentered(const std::string& text) {
    auto windowWidth = ImGui::GetWindowSize().x;
    auto textWidth   = ImGui::CalcTextSize(text.c_str()).x;
    ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
    ImGui::Text("%s", text.c_str());
}

bool LoadTextureFromFile(const char* filename, GLuint* out_texture, int* out_width, int* out_height) {
    // Load from file
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create a OpenGL texture identifier
    GLuint image_texture;
    glGenTextures(1, &image_texture);
    glBindTexture(GL_TEXTURE_2D, image_texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); // Same

    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image_width, image_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_data);
    stbi_image_free(image_data);

    *out_texture = image_texture;
    *out_width = image_width;
    *out_height = image_height;

    return true;
}

// realtime sample from implot_demo
struct ScrollingBuffer {
    int MaxSize;
    int Offset;
    ImVector<ImVec2> Data;
    ScrollingBuffer(int max_size = 2000) {
        MaxSize = max_size;
        Offset  = 0;
        Data.reserve(MaxSize);
    }
    void AddPoint(float x, float y) {
        if (Data.size() < MaxSize)
            Data.push_back(ImVec2(x,y));
        else {
            Data[Offset] = ImVec2(x,y);
            Offset =  (Offset + 1) % MaxSize;
        }
    }
    void Erase() {
        if (Data.size() > 0) {
            Data.shrink(0);
            Offset  = 0;
        }
    }
};

// utility structure for realtime plot
struct RollingBuffer {
    float Span;
    ImVector<ImVec2> Data;
    RollingBuffer() {
        Span = 10.0f;
        Data.reserve(2000);
    }
    void AddPoint(float x, float y) {
        float xmod = fmodf(x, Span);
        if (!Data.empty() && xmod < Data.back().x)
            Data.shrink(0);
        Data.push_back(ImVec2(xmod, y));
    }
};

void Demo_RealtimePlots() {
    static ScrollingBuffer sdata1, sdata2;
    static RollingBuffer   rdata1, rdata2;
    ImVec2 mouse = ImGui::GetMousePos();
    static float t = 0;
    t += ImGui::GetIO().DeltaTime;
    sdata1.AddPoint(t, mouse.x * 0.0005f);
    rdata1.AddPoint(t, mouse.x * 0.0005f);
    sdata2.AddPoint(t, mouse.y * 0.0005f);
    rdata2.AddPoint(t, mouse.y * 0.0005f);

    static float history = 30.0f;
    //ImGui::SliderFloat("History",&history,1,30,"%.1f s");
    rdata1.Span = history;
    rdata2.Span = history;

    static ImPlotAxisFlags flags = ImPlotAxisFlags_NoTickLabels;

    if (ImPlot::BeginPlot("##Scrolling", ImVec2(-1,450))) {
        ImPlot::SetupAxes(NULL, NULL, flags, flags);
        ImPlot::SetupAxisLimits(ImAxis_X1,t - history, t, ImGuiCond_Always);
        ImPlot::SetupAxisLimits(ImAxis_Y1,0,1);
        ImPlot::SetNextFillStyle(IMPLOT_AUTO_COL,0.5f);
        ImPlot::PlotShaded("Mouse X", &sdata1.Data[0].x, &sdata1.Data[0].y, sdata1.Data.size(), -INFINITY, 0, sdata1.Offset, 2 * sizeof(float));
        ImPlot::PlotLine("Mouse Y", &sdata2.Data[0].x, &sdata2.Data[0].y, sdata2.Data.size(), 0, sdata2.Offset, 2*sizeof(float));
        ImPlot::EndPlot();
    }
    if (ImPlot::BeginPlot("##Rolling", ImVec2(-1,450))) {
        ImPlot::SetupAxes(NULL, NULL, flags, flags);
        ImPlot::SetupAxisLimits(ImAxis_X1,0,history, ImGuiCond_Always);
        ImPlot::SetupAxisLimits(ImAxis_Y1,0,1);
        ImPlot::PlotLine("Mouse X", &rdata1.Data[0].x, &rdata1.Data[0].y, rdata1.Data.size(), 0, 0, 2 * sizeof(float));
        ImPlot::PlotLine("Mouse Y", &rdata2.Data[0].x, &rdata2.Data[0].y, rdata2.Data.size(), 0, 0, 2 * sizeof(float));
        ImPlot::EndPlot();
    }
}
